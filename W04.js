//  We've created an App object (a set of key value pairs) to hold the applcation code.
//  The App object shows how to create a JavaScript object and includes
//  examples of standard programming constructs in JavaScript. 
//  The goal is provide many useful examples in a single code file. 
//
//  When you modify the application, use different ids for your HTML elements.
//  Do not use length and width. 

var App = {
  launch: function () {
    App.getName();
    App.getOrg();
    App.getRows();
    App.getPerRow();
    App.getSeats();
    App.getEstimate();
    App.displayExploreButtons();
    App.showExample();
    App.rememberClicks();
  },
  getName: function () {
    let answer = prompt("What is your name", "Notorious BIG");
    if (answer != null) {
      $("#name").html(answer); // $ = jQuery object, uses CSS selectors
      sessionStorage.name = answer
    }
  },
  getOrg: function () {
    const s = sessionStorage.org + ", what is your Organization's name"
    let answer = prompt(s, "Wu Tang Clan");
    if (answer != null) {
      $("#org").html(answer);  // setter
    }
  },
  getRows: function () {
    let answer = prompt("How many rows would you like to reserve?", 5);
    if (answer != null) {
      //document.getElementById("rows").innerHTML = answer;
      $('#rows').html(answer);   // either double or single tick marks designate strings
    }
  },
  getPerRow: function () {
    let answer = prompt("How many seats per row would you like to reserve?", 5);
    if (answer != null) {
      $('#perRow').html(answer);  // html method works as a getter and a setter
    }
  },
  getSeats: function () {
    //let inputRows = parseFloat(document.getElementById("rows").innerHTML);
    //let inputLength = parseFloat(document.getElementById("length").innerHTML);
    //let answer = Area.calculateArea(inputWidth, inputLength); // do some checks on the inputs
    //document.getElementById("area").innerHTML = answer;
    let inputRows = parseFloat($('#rows').html());
    let inputPerRow = parseFloat($('#perRow').html());
    let answer = App.calculateSeats(inputRows, inputPerRow); // do some checks on the inputs
    $("#seats").html(answer);
    $(".displayText").css('display', 'inline-block');  //overwrites display: hidden to make it visible 
    alert("You have " + answer + " seats.");
  },
  calculateSeats: function (givenRows, givenPerRow) {
    if (typeof givenRows !== 'number' || typeof givenPerRow !== 'number') {
      throw Error('The given argument is not a number');
    }

    const minRows = 1;
    const minPerRow = 1;
    const maxRows = 100;
    const maxPerRows = 100;

    // check the first argument.................
    let rows  // undefined
    if (givenRows < minRows) {
      rows = 0;
    }
    else if (givenRows > maxRows) {
      rows = 0;
    }
    else {
      rows = givenRows;
    }

    //check the second argument ...................
    let perRow
    if (givenPerRow < minPerRow) {
      perRow = minPerRow;
    }
    else if (givenPerRow > maxPerRows) {
      perRow = maxPerRows;
    }
    else {
      perRow = givenPerRow;
    }

    // calculate the answer and store in a local variable so we can watch the value
    let seats = rows * perRow;
    // return the result of our calculation to the calling function
    return seats;
  },
    
  
  getEstimate: function () {
    let seats = parseFloat(document.getElementById("seats").innerHTML);
    let ct;
    if (seats < 1) { ct = 0; }
    else { ct = seats }; // estimate 1 per square mile
    // document.getElementById("count").innerHTML = count;
    $("#count").html(ct);
    alert("You could have about " + ct + " seats.");
    $("#count").css("color", "blue");
    $("#count").css("background-color", "yellow");
  },
  

  //Leave unmodified
  showExample: function () {
    document.getElementById("displayPlace").innerHTML = "";
    let totalCount = parseFloat($("#count").html());
    for (var i = 0; i < totalCount; i++) {
      App.addImage(i);
    }
  },
  addImage: function (icount) {
    var imageElement = document.createElement("img");
    imageElement.id = "image" + icount;
    imageElement.class = "picture";
    imageElement.style.maxWidth = "90px";
    var displayElement = document.getElementById("displayPlace");
    displayElement.appendChild(imageElement);
    document.getElementById("image" + icount).src = "bearcat.jpg";
  },
  displayExploreButtons: function () {
    $(".displayExploreButtons").css('display', 'block');  //overwrites display: hidden to make it visible 
  },
  exploreHtml: function () {
    alert("Would you like to learn more? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the Elements tab.\n\n" +
      "Hit CTRL-F and search for displayPlace to see the new image elements you added to the page.\n")
  },
  exploreCode: function () {
    alert("Would you like explore the running code? \n\n Run the app in Chrome.\n\n" +
      "Right-click on the page, and click Inspect. Click on the top-level Sources tab.\n\n" +
      "In the window on the left, click on the .js file.\n\n" +
      "In the window in the center, click on the line number of the App.getFirstName() call to set a breakpoint.\n\n" +
      "Click on it again to remove the breakpoint, and one more time to turn it back on.\n\n" +
      "Up on the web page, click the main button to launch the app.\n\n" +
      "Execution of the code will stop on your breakpoint.\n\n" +
      "Hit F11 to step into the App.getFirstName() function.\n" +
      "Hit F10 to step over the next function call.\n\n" +
      "As you hit F11 and step through your code, the values of local variables appear beside your code - very helpful in debugging.\n\n" +
      "Caution: Hitting F11 in VS Code will make your top-level menu disapper. Hit F11 again to bring it back.\n"
    )
  },
  rememberClicks: function () {
    if (localStorage.getItem("clicks")) { // use getter
      const value = Number(localStorage.clicks) + 1  // or property
      localStorage.setItem("clicks", value)  // use setter
    } else {
      localStorage.clicks = 1 // or property
    }
    s = "You have clicked this button " + localStorage.clicks + " times"
    $("#clicks").html(s) // display forever clicks 
  }
};

